<?php
declare(strict_types=1);

namespace Dotgroup\NeoLMS\UserModule;

use Dotgroup\NeoLMS\UserModule\Action\UserListAction;
use Dotgroup\NeoLMS\Contract\ModuleInterface;

class Manifest implements ModuleInterface
{
    public function routes(): array
    {
        return [
            ['verb' => 'GET', 'route' => '/user/{paramA}/{paramB}', 'class' => UserListAction::class],
        ];
    }

    public function middlewares(): array
    {
        return [];
    }
}