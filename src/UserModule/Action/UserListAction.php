<?php

namespace Dotgroup\NeoLMS\UserModule\Action;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UserListAction
{
    public function __invoke(Request $request, Response $response, $args)
    {
        return $response->withStatus(200)->write('Args:'.$args['paramA'].', '.$args['paramB']);
    }
}