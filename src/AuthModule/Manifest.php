<?php
declare(strict_types=1);

namespace Dotgroup\NeoLMS\AuthModule;

use Dotgroup\NeoLMS\AuthModule\Action\LoginAction;
use Dotgroup\NeoLMS\Contract\ModuleInterface;
use Dotgroup\NeoLMS\AuthModule\Middleware\RedirectNotAllowed;
use Dotgroup\NeoLMS\AuthModule\Middleware\VerifyJWTHeader;

class Manifest implements ModuleInterface
{
    public function routes(): array
    {
        return [
            ['verb' => 'GET', 'route' => '/login', 'class' => LoginAction::class],
        ];
    }

    public function middlewares(): array
    {
        return [
          ['class' => VerifyJWTHeader::class, 'priority'=> -1],
          ['class' => RedirectNotAllowed::class, 'priority'=> -2],
        ];
    }
}