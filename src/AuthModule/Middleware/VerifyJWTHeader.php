<?php
/**
 * Created by PhpStorm.
 * User: luiznunes
 * Date: 01/03/17
 * Time: 14:48
 */

namespace Dotgroup\NeoLMS\AuthModule\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class VerifyJWTHeader
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
        $response->getBody()->write('BEFORE ');
        $response = $next($request, $response);
        $response->getBody()->write(' AFTER');

        return $response;
    }
}