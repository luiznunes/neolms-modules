<?php

namespace Dotgroup\NeoLMS\AuthModule\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

class RedirectNotAllowed
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next) {
        $response->getBody()->write('NOT ALLOWED ');
        $response = $next($request, $response);
        $response->getBody()->write(' NOT ALLOWED');

        return $response;
    }
}