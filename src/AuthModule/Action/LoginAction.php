<?php
/**
 * Created by PhpStorm.
 * User: luiznunes
 * Date: 01/03/17
 * Time: 14:44
 */

namespace Dotgroup\NeoLMS\AuthModule\Action;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class LoginAction
{
    public function __invoke(Request $request, Response $response, $args)
    {
        return $response->withStatus(200)->write('http://clean.local/home');
    }
}